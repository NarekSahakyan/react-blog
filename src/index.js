import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/Common/Header';
import './index.css'
import List from './components/List/List'
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Detail from './components/Detail/Detail'
import NotFound from './components/Notfound/NotFound'

const App = () => {
    return(
        <BrowserRouter>
             <div>
                <Header />
                <Switch>
                    <Route path="/" component={List} exact/>
                    <Route  path="/currency/:id" component={Detail} exact/>
                    <Route component={NotFound}/>
                </Switch>
            </div>
        </BrowserRouter>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));



  



