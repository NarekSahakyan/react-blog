import React from 'react';
import './Search.css';
import {API_URL} from '../../Config';
import{handleResponse} from '../../Helper';
import Loading from '../Common/Loading'
import { withRouter } from 'react-router-dom';

class Search extends React.Component {
    constructor(){
        super();
        this.state= {
            searchResults : [],
            searchQuery : '',
            loading : false, 
        }
        this.handleRedirect = this.handleRedirect.bind(this)
        this.handleChange = this.handleChange.bind(this);
        this.renderSearchResults = this.renderSearchResults.bind(this)
    }

    handleChange(event){
        const searchQuery = event.target.value;
        this.setState({searchQuery,})

        if (!searchQuery) {
            return '';
          }

          this.setState({ loading: true });

        fetch(`${API_URL}autocomplete?searchQuery=${searchQuery}`)
        .then(handleResponse)
        .then((result) => {
            this.setState({ 
                loading: false,
                searchResults : result, 
            });
        });
    }

    handleRedirect(currencyId){
        this.setState({
            searchResults : [],
            searchQuery : '',
        })
        this.props.history.push(`/currency/${currencyId}`);
    }

    renderSearchResults(){
        const {searchQuery,searchResults} = this.state;
        if(!searchQuery){
            return '';
        }
        if(searchResults.length>0){
            return(
                <div className="Search-result-container">
                    {searchResults.map(result=>(
                        <div 
                            className="Search-result" 
                            key={result.id} 
                            onClick={() => this.handleRedirect(result.id)}
                        >
                            {result.name} ({result.symbol})
                        </div>
                    ))}
                </div>
            )
        }
    }

    render(){
        const {loading} = this.state;
        return(
            <div className="Search">
                <span className="Search-icon" />
                <input
                    className="Search-input"
                    type="text"
                    placeholder="Currency name"
                    onChange = {this.handleChange}
                />
                {loading && 
                    <div className="Search-loading">
                    <Loading
                      width='12px'
                      height='12px'
                    />
                    </div>
                }
                {this.renderSearchResults()}
            </div>
        )
    }
}

export default withRouter(Search);