import React from 'react';
import './Pagination.css'
import PropTypes from 'prop-types';

const Pagination = (props) =>{
    const {handlePaginationClick,page,totalPages} = props
    return(
        <div className="Pagination">
            <button 
                disabled={page<=1} 
                className="Pagination-button"
                onClick={()=>handlePaginationClick("prev")}>
                &larr;
            </button>
            <span className="Pagination-info">page <b>{page}</b> of <b>{totalPages}</b></span>
            <button 
                disabled={page===totalPages} 
                className="Pagination-button" 
                onClick={()=>handlePaginationClick("next")}>
                &rarr;
            </button>
        </div>
    )
}

Pagination.propTypes = {
    page : PropTypes.number.isRequired,
    totalPages : PropTypes.number.isRequired,
    handlePaginationClick : PropTypes.func.isRequired,
}   

export default Pagination;