import React from 'react';
import {API_URL} from '../../Config';
import { handleResponse,renderChangePercent } from '../../Helper';
import Loading from '../Common/Loading';
import Table from './Table';
import Pagination from './Pagination';

class List extends React.Component{
    constructor(){
        super();

        this.state = {
            loading : false,
            currencies : [],
            error: null,
            totalPages : 0,
            page: 1,
        }
        this.handlePaginationClick = this.handlePaginationClick.bind(this)
    }

    fetchCurrencies(){
        this.setState({loading : true})
        const {page} = this.state;

        fetch(`${API_URL}cryptocurrencies?page=${page}&perPage=20`)
        .then(handleResponse)
        .then(data=>{
            const {currencies,totalPages} = data;
            this.setState({
                loading : false,
                currencies,
                totalPages
            })
        })
        .catch(error=>{
            this.setState({
                loading : false,
                error : error.errorMessage
            })
        })
    }

    componentDidMount(){
        this.fetchCurrencies();
    }


    handlePaginationClick(direction){
        let nextPage = this.state.page;
        nextPage = direction === 'next' ? nextPage + 1 : nextPage - 1;
        this.setState({page : nextPage},()=>{this.fetchCurrencies()})
    }

    render(){
        const {loading,currencies,error,page,totalPages} = this.state;
        if(loading){
           return <Loading/>
        }
        if(error){
            return <div className="error">{error}</div>
        }
        return(
            <div>
                <Table
                    currencies={currencies} 
                    renderChangePercent={renderChangePercent}
                 />

                <Pagination 
                    handlePaginationClick={this.handlePaginationClick} 
                    page={page} 
                    totalPages={totalPages}
                />
            </div>
            
        )
    }
}

export default List;